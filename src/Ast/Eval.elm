module Ast.Eval exposing (eval)

import Ast.Error exposing (..)
import Ast.Exp exposing (..)
import Dict exposing (Dict)


type alias Env =
    Dict String Exp


eval : Env -> Exp -> Result Error Exp
eval env ast =
    let
        evalEnv =
            eval env
    in
    case ast of
        Literal _ ->
            Ok ast

        OpApply op list ->
            List.map evalEnv list
                |> sequenceR
                |> Result.andThen (evalOp op)

        ExpList li ->
            case li |> List.map evalEnv |> sequenceR of
                Ok list ->
                    case list of
                        head :: [] ->
                            Ok head

                        _ ->
                            Ok (ExpList list)

                Err err ->
                    Err err

        Quote quote ->
            Ok ast

        Define sym exp ->
            Result.map (\evaluated -> Define sym evaluated) (evalEnv exp)

        Ref (Symbol str) ->
            case Dict.get str env of
                Just exp ->
                    Ok exp

                Nothing ->
                    Err (NotDefined <| str ++ " is not defined")

        If exp a b ->
            case evalEnv exp of
                Ok (Literal (Boolean bool)) ->
                    if bool then
                        evalEnv a

                    else
                        evalEnv b

                _ ->
                    Err (BadArgument "first argument must be a boolean value")

        Lambda symbols exp ->
            Ok (Lambda symbols exp)

        Apply exp exps ->
            Ok (Ref (Symbol "Todo: eval Apply"))


evalOp : Op -> List Exp -> Result Error Exp
evalOp op explist =
    let
        isInteger =
            \exp ->
                case exp of
                    Literal (Integer i) ->
                        Just i

                    _ ->
                        Nothing

        intList : List Int
        intList =
            explist |> List.filterMap isInteger
    in
    case intList of
        [] ->
            Err <| BadArgument "No arguments"

        head :: [] ->
            Err <| BadArgument "Too few arguments"

        head :: [ tail ] ->
            case op of
                Add ->
                    head + tail |> (Ok << Literal << Integer)

                Mul ->
                    head * tail |> (Ok << Literal << Integer)

                Sub ->
                    head - tail |> (Ok << Literal << Integer)

                Mod ->
                    if head == 0 then
                        Err CannotDivideByZero

                    else
                        modBy head tail |> (Ok << Literal << Integer)

                Div ->
                    if List.member 0 intList then
                        Err CannotDivideByZero

                    else
                        head - tail |> (Ok << Literal << Integer)

                Gt ->
                    head > tail |> (Ok << Literal << Boolean)

        head :: rest ->
            case op of
                Add ->
                    intList |> List.sum |> (Ok << Literal << Integer)

                Mul ->
                    rest |> List.foldr (*) head |> (Ok << Literal << Integer)

                Sub ->
                    rest |> List.foldr (\a acc -> acc - a) head |> (Ok << Literal << Integer)

                Div ->
                    if List.member 0 intList then
                        Err CannotDivideByZero

                    else
                        intList |> List.foldr (//) 1 |> (Ok << Literal << Integer)

                Mod ->
                    Err <| BadArgument "Mod takes only two arguments"

                Gt ->
                    Err <| BadArgument "GT takes only two arguments"



-- Util


traverseM : (a -> Maybe b) -> List a -> Maybe (List b)
traverseM f =
    let
        step e acc =
            case f e of
                Just something ->
                    Maybe.map ((::) something) acc

                Nothing ->
                    Nothing
    in
    List.foldr step (Just [])


sequenceM : List (Maybe a) -> Maybe (List a)
sequenceM =
    traverseM identity


traverseR : (a -> Result s b) -> List a -> Result s (List b)
traverseR f =
    let
        step e acc =
            case f e of
                Ok something ->
                    Result.map ((::) something) acc

                Err err ->
                    Err err
    in
    List.foldr step (Ok [])


sequenceR : List (Result s a) -> Result s (List a)
sequenceR =
    traverseR identity

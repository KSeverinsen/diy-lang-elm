module Ast.Parser exposing (parse)

import Ast.Error exposing (Error(..))
import Ast.Exp exposing (..)
import Parser as Parser exposing ((|.), (|=), Parser, Problem(..), Step)
import Set


parse : String -> Result Error Exp
parse str =
    case str of
        "" ->
            Err IncompleteExpression

        _ ->
            Result.mapError Ast.Error.fromDeadEnds (Parser.run parser str)


parser : Parser Exp
parser =
    Parser.succeed identity
        |= exp
        |. Parser.end


exp : Parser Exp
exp =
    Parser.succeed identity
        |. spaces
        |= Parser.lazy
            (\_ ->
                Parser.oneOf
                    [ define
                    , lambda
                    , if_
                    , listParser
                    , refParser
                    , literalParser
                    , quote
                    , opApply
                    ]
            )
        |. spaces


listParser : Parser Exp
listParser =
    Parser.succeed ExpList
        |. Parser.symbol "("
        |. spaces
        |= Parser.loop [] listHelp
        |. spaces
        |. Parser.symbol ")"


listHelp : List Exp -> Parser (Step (List Exp) (List Exp))
listHelp source =
    Parser.oneOf
        [ Parser.succeed
            (\inn -> Parser.Loop (inn :: source))
            |= exp
            |. spaces
        , Parser.succeed ()
            |> Parser.map (\_ -> Parser.Done (List.reverse source))
        ]


define : Parser Exp
define =
    Parser.succeed Define
        |. Parser.keyword "define"
        |. spaces
        |= symbol
        |. spaces
        |= exp
        |. spaces


if_ : Parser Exp
if_ =
    Parser.succeed If
        |. Parser.keyword "if"
        |= exp
        |= exp
        |= exp


quote : Parser Exp
quote =
    Parser.succeed Quote
        |. Parser.symbol "'"
        |= exp


opApply : Parser Exp
opApply =
    Parser.succeed OpApply
        |= op
        |. spaces
        |= Parser.loop [] listHelp
        |. spaces


op : Parser Op
op =
    Parser.oneOf
        [ Parser.succeed Mul
            |. Parser.symbol "*"
        , Parser.succeed Add
            |. Parser.symbol "+"
        , Parser.succeed Sub
            |. Parser.symbol "-"
        , Parser.succeed Mod
            |. Parser.symbol "%"
        , Parser.succeed Div
            |. Parser.symbol "/"
        , Parser.succeed Gt
            |. Parser.symbol ">"
        ]


lambda : Parser Exp
lambda =
    Parser.succeed Lambda
        |. Parser.keyword "lambda"
        |. spaces
        |. Parser.symbol "("
        |. spaces
        |= symbols
        |. spaces
        |. Parser.symbol ")"
        |. spaces
        |= exp


literalParser : Parser Exp
literalParser =
    Parser.succeed Literal
        |= Parser.oneOf
            [ booleanParser
            , integerParser
            ]


refParser : Parser Exp
refParser =
    Parser.succeed Ref
        |= symbol


symbol : Parser Symbol
symbol =
    Parser.succeed Symbol
        |= Parser.variable
            { start = Char.isLower
            , inner = \c -> Char.isAlphaNum c
            , reserved = Set.fromList [ "if", "define", "lambda", "#t", "#f" ]
            }


symbols : Parser (List Symbol)
symbols =
    Parser.loop [] symbolsHelp


symbolsHelp : List Symbol -> Parser (Step (List Symbol) (List Symbol))
symbolsHelp revSym =
    Parser.oneOf
        [ Parser.succeed (\sym -> Parser.Loop (sym :: revSym))
            |= symbol
            |. spaces
        , Parser.succeed ()
            |> Parser.map (\_ -> Parser.Done (List.reverse revSym))
        ]


booleanParser : Parser Value
booleanParser =
    Parser.oneOf
        [ Parser.succeed (Boolean True)
            |. Parser.keyword "#t"
        , Parser.succeed (Boolean False)
            |. Parser.keyword "#f"
        ]


integerParser : Parser Value
integerParser =
    Parser.succeed Integer
        |= Parser.int


spaces : Parser ()
spaces =
    Parser.oneOf
        [ comment
        , Parser.spaces
        ]


comment : Parser ()
comment =
    Parser.oneOf
        [ lineComment ";;"
        , lineComment ";"
        ]


lineComment : String -> Parser ()
lineComment str =
    Parser.symbol str
        |. Parser.chompUntilEndOr "\n"



-- findMatchingParen : String -> Maybe String
-- findMatchingParen source =
--     let
--         helperF : Char -> ( Maybe String, String ) -> ( Maybe String, String )
--         helperF c ( res, acc ) =
--             let
--                 updated =
--                     String.cons c acc
--             in
--             if isBalanced updated then
--                 ( Just updated, updated )
--             else
--                 ( res, updated )
--     in
--     String.foldl helperF ( Nothing, "" ) source
--         |> Tuple.first
--         |> Maybe.map String.reverse
-- isBalanced : String -> Bool
-- isBalanced source =
--     let
--         countParen : Char -> Int -> Int
--         countParen =
--             \c acc ->
--                 if c == '(' then
--                     acc + 1
--                 else if c == ')' then
--                     acc - 1
--                 else
--                     acc
--     in
--     if String.foldr countParen 0 source == 0 then
--         True
--     else
--         False

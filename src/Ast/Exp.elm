module Ast.Exp exposing (Exp(..), Op(..), Symbol(..), Value(..), toString)


type Exp
    = Lambda (List Symbol) Exp
    | If Exp Exp Exp
    | Define Symbol Exp
    | Apply Exp (List Exp)
    | OpApply Op (List Exp)
    | Literal Value
    | Ref Symbol
    | ExpList (List Exp)
    | Quote Exp


type Symbol
    = Symbol String


type Value
    = Integer Int
    | Boolean Bool


type Op
    = Mul
    | Add
    | Sub
    | Mod
    | Div
    | Gt


toString : Exp -> String
toString e =
    case e of
        Lambda symbols exp ->
            "( " ++ listToString symbols symbolToString ++ " -> " ++ toString exp ++ " )"

        If exp1 exp2 exp3 ->
            "If " ++ toString exp1 ++ " " ++ toString exp2 ++ " " ++ toString exp3

        Define symbol exp ->
            "Define " ++ symbolToString symbol ++ " " ++ toString exp

        Apply exp exps ->
            "Apply " ++ toString exp ++ listToString exps toString

        OpApply op exps ->
            opToString op ++ " " ++ listToString exps toString

        Literal value ->
            valueToString value

        Ref symbol ->
            "Ref " ++ symbolToString symbol

        ExpList exps ->
            "( " ++ listToString exps toString ++ ")"

        Quote exp ->
            "Quote " ++ toString exp


listToString : List a -> (a -> String) -> String
listToString l func =
    List.map func l |> List.foldr (\a b -> a ++ " " ++ b) ""


symbolToString : Symbol -> String
symbolToString (Symbol s) =
    s


valueToString : Value -> String
valueToString lit =
    case lit of
        Integer i ->
            String.fromInt i

        Boolean b ->
            if b then
                "#t"

            else
                "#f"


opToString : Op -> String
opToString op =
    case op of
        Mul ->
            "Mul"

        Add ->
            "Add"

        Sub ->
            "Sub"

        Mod ->
            "Mod"

        Div ->
            "Div"

        Gt ->
            "Gt"

module Ast.Error exposing (Error(..), fromDeadEnds)

import Parser exposing (Problem(..))


type Error
    = IncompleteExpression
    | ExpectedEOF
    | DeadEnds String
    | NotImplemented
    | EvalError String
    | CannotDivideByZero
    | BadArgument String
    | NotDefined String


fromDeadEnds : List Parser.DeadEnd -> Error
fromDeadEnds err =
    let
        problems =
            err
                |> List.map .problem
    in
    if List.member ExpectingEnd problems then
        ExpectedEOF

    else if List.member (ExpectingSymbol ")") problems then
        IncompleteExpression

    else
        DeadEnds "DeadEnd"

module Main exposing (Model, Msg(..), init, main, update, view)

import Ast.Error as Ast
import Ast.Eval as Ast
import Ast.Exp as Exp exposing (Exp(..), Symbol(..))
import Ast.Parser as Ast
import Browser
import Dict exposing (Dict)
import Html exposing (Html, div, h1, img, text)
import Html.Attributes exposing (src)
import Html.Events as Events



---- MODEL ----


type alias Model =
    { input : List String
    , ast : Result Ast.Error Exp
    , environment : Dict String Exp
    }


init : ( Model, Cmd Msg )
init =
    ( { input = []
      , ast = Err Ast.IncompleteExpression
      , environment = Dict.empty
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | InputReceived String
    | ParseButtonClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        InputReceived str ->
            ( { model | input = str :: List.drop 1 model.input }, Cmd.none )

        ParseButtonClicked ->
            let
                parsed : Result Ast.Error Exp
                parsed =
                    model.input
                        |> List.head
                        |> Result.fromMaybe Ast.IncompleteExpression
                        |> Result.andThen Ast.parse

                evaluated =
                    parsed
                        |> Result.andThen (Ast.eval model.environment)
            in
            case evaluated of
                Ok (Define (Symbol s) exp) ->
                    ( { model | ast = parsed, input = List.append (List.take 1 model.input) model.input, environment = Dict.insert s exp model.environment }, Cmd.none )

                _ ->
                    ( { model | ast = parsed, input = List.append (List.take 1 model.input) model.input }, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ img [ src "./logo.svg" ] []
        , h1 [] [ text "diy-lang parser" ]
        , viewInputHistory model.input
        , Html.textarea [ Events.onInput InputReceived ] []
        , Html.button [ Events.onClick ParseButtonClicked ] [ Html.text "Eval" ]
        , Html.div []
            [ viewAst "Ast:" model.ast
            , viewAst "Eval:" (model.ast |> Result.andThen (Ast.eval model.environment))
            ]
        ]


viewInputHistory : List String -> Html Msg
viewInputHistory input =
    ---- PROGRAM ----
    Html.div []
        [ Html.p [ Html.Attributes.style "color" "green" ] [ Html.text "Input:" ]
        , Html.ul [] <|
            (input
                |> List.drop 1
                |> List.take 3
                |> List.reverse
                |> List.map (\a -> Html.li [] [ Html.text a ])
            )
        ]


viewAst : String -> Result Ast.Error Exp -> Html Msg
viewAst header ast =
    Html.div []
        [ Html.p [ Html.Attributes.style "color" "green" ] [ Html.text header ]
        , Html.p [] [ Result.toMaybe ast |> Maybe.map Exp.toString |> Maybe.withDefault "Error" |> Html.text ]
        ]


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }

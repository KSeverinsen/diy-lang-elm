module ParserTest exposing (test_1_parsing)

import Ast.Error exposing (ParseError(..))
import Ast.Exp exposing (Exp(..), Symbol(..), Value(..))
import Ast.Parser exposing (..)
import Expect
import Fuzz
import Parser
import Test exposing (..)


test_1_parsing : Test
test_1_parsing =
    describe "Parsing"
        [ describe "TEST 1.1: Parsing a single symbol."
            {- Parsing a single symbol.
               Symbols are represented by text strings. Parsing a single atom should
               result in an AST consisting of only that symbol.
            -}
            [ test "foo equal foo" <|
                \_ ->
                    Expect.equal (Ok (Ref (Symbol "foo"))) (parse "foo")
            ]
        , describe "TEST 1.2: Parsing single booleans."
            {- Booleans are the special symbols #t and #f. In the ASTs they are
               represented by Python's True and False, respectively."""
               assert_equals(True, parse('#t'))
               assert_equals(False, parse('#f'))
            -}
            [ test "#t equal True" <|
                \_ ->
                    Expect.equal (Ok (Literal (Boolean True))) (parse "#t")
            , test "#f equal False" <|
                \_ ->
                    Expect.equal (Ok (Literal (Boolean False))) (parse "#f")
            ]
        , describe "TEST 1.3: Parsing single integer."
            {- TEST 1.4: Parsing list of only symbols.
               A list is represented by a number of elements surrounded by parens. Python
               lists are used to represent lists as ASTs.
               Tip: The useful helper function `find_matching_paren` is already provided
               in `parse.py`.
            -}
            [ test "parse 42" <|
                \_ ->
                    Expect.equal (Ok (Literal (Integer 42))) (parse "42")
            , test "parse 1337" <|
                \_ ->
                    Expect.equal (Ok (Literal (Integer 1337))) (parse "1337")
            ]
        , describe "TEST 1.4: Parsing list of only symbols."
            {- A list is represented by a number of elements surrounded by parens. Python
               lists are used to represent lists as ASTs.
               Tip: The useful helper function `find_matching_paren` is already provided
               in `parse.py`.
            -}
            [ test "(foo bar baz) equals [foo bar baz]" <|
                \_ ->
                    Expect.equal (Ok (ExpList [ Ref (Symbol "foo"), Ref (Symbol "bar"), Ref (Symbol "baz") ])) (parse "(foo bar baz)")
            , test "() equals []" <|
                \_ ->
                    Expect.equal (Ok (ExpList [])) (parse "()")
            ]
        , describe "TEST 1.5: Parsing a list containing different types."
            {- Parsing a list containing different types.
               When parsing lists, make sure each of the sub-expressions are also parsed
               properly
            -}
            [ test "parse (foo 123 456)" <|
                \_ ->
                    parse "(foo 123 456)"
                        |> Expect.equal (Ok (ExpList [ Ref (Symbol "foo"), Literal (Integer 123), Literal (Integer 456) ]))
            , test "parse (foo 123 #t)" <|
                \_ ->
                    parse "(foo 123 #t)"
                        |> Expect.equal (Ok (ExpList [ Ref (Symbol "foo"), Literal (Integer 123), Literal (Boolean True) ]))
            ]
        , describe "TEST 1.6: Parsing should also handle nested lists properly."
            [ test "(foo 1 (bar))" <|
                \_ ->
                    parse "(foo 1 (bar))"
                        |> Expect.equal (Ok (ExpList [ Ref (Symbol "foo"), Literal (Integer 1), ExpList [ Ref (Symbol "bar") ] ]))
            , test "(foo 1 (bar 1 2))" <|
                \_ ->
                    parse "(foo 1 (bar 1 2))"
                        |> Expect.equal (Ok (ExpList [ Ref (Symbol "foo"), Literal (Integer 1), ExpList [ Ref (Symbol "bar"), Literal (Integer 1), Literal (Integer 2) ] ]))
            , test "(foo (bar ((#t)) x) (baz y))" <|
                \_ ->
                    parse "(foo (bar ((#t)) x) (baz y))"
                        |> Expect.equal
                            (Ok
                                (ExpList
                                    [ Ref (Symbol "foo")
                                    , ExpList [ Ref (Symbol "bar"), ExpList [ ExpList [ Literal (Boolean True) ] ], Ref (Symbol "x") ]
                                    , ExpList [ Ref (Symbol "baz"), Ref (Symbol "y") ]
                                    ]
                                )
                            )
            ]
        , describe "TEST 1.7: The proper exception should be raised if the expression is incomplete"
            [ test "(foo (bar x y)" <|
                \_ ->
                    parse "(foo (bar x y)"
                        |> Expect.equal (Err IncompleteExpression)
            ]
        , describe "TEST 1.8: Another exception is raised if the expression is too large. The parse function expects to receive only one single expression. Anything more than this, should result in the proper exception."
            [ test "(foo (bar x y)))" <|
                \_ ->
                    parse "(foo (bar x y)))"
                        |> Expect.equal (Err ExpectedEOF)
            ]
        , describe "TEST 1.9: Excess whitespace should be removed."
            [ test "(program    with   much        whitespace)" <|
                \_ ->
                    parse "(program    with   much        whitespace)"
                        |> Expect.equal (Ok (ExpList [ Ref (Symbol "program"), Ref (Symbol "with"), Ref (Symbol "much"), Ref (Symbol "whitespace") ]))
            ]
        , describe "TEST If"
            [ test "(if #t 42 something)" <|
                \_ ->
                    parse "(if #t 42 something)"
                        |> Expect.equal
                            (Ok
                                (ExpList
                                    [ If (Literal (Boolean True)) (Literal (Integer 42)) (Ref (Symbol "something")) ]
                                )
                            )
            ]
        , describe "TEST If (I'm insane)"
            [ test "(if #t 42 something)" <|
                \_ ->
                    parse """(if #t 42 else)"""
                        |> Expect.equal
                            (Ok
                                (ExpList
                                    [ If (Literal (Boolean True)) (Literal (Integer 42)) (Ref (Symbol "else")) ]
                                )
                            )
            ]
        , describe "TEST 1.10: Multi line program"
            [ test "(define variable (if #t 42 (something else)))" <|
                \_ ->
                    let
                        program =
                            """
                                (define variable
                                    (if #t
                                        42
                                        (something else)))
                            """
                    in
                    parse program
                        |> Expect.equal
                            (Ok
                                (ExpList
                                    [ Define (Symbol "variable")
                                        (ExpList
                                            [ If (Literal (Boolean True))
                                                (Literal (Integer 42))
                                                (ExpList [ Ref (Symbol "something"), Ref (Symbol "else") ])
                                            ]
                                        )
                                    ]
                                )
                            )
            ]

        -- , describe "TEST 1.10: All comments should be stripped away as part of the parsing."
        --     [ test "(define variable (if #t 42 (something else)))" <|
        --         \_ ->
        --             let
        --                 program =
        --                     """
        --                         ;; this first line is a comment
        --                         (define variable
        --                             ; here is another comment
        --                             (if #t
        --                                 42 ; inline comment!
        --                                 (something else)))
        --                     """
        --             in
        --             parse program
        --                 |> Expect.equal (Ok (Define (Symbol "variable") (ExpList [])))
        --     ]
        , describe """TEST 1.12: Quoting is a shorthand syntax for calling the `quote` form."""
            [ test "'foo -> (quote foo)" <|
                \_ ->
                    parse "'foo"
                        |> Expect.equal (Ok (Quote (Ref (Symbol "foo"))))
            , test "'(foo bar) -> (quote (foo bar))" <|
                \_ ->
                    parse "'(foo bar)"
                        |> Expect.equal (Ok (Quote (ExpList [ Ref (Symbol "foo"), Ref (Symbol "bar") ])))
            , test "TEST 1.13: Nested quotes should work as expected" <|
                \_ ->
                    parse "''''foo"
                        |> Expect.equal (Ok (Quote (Quote (Quote (Quote (Ref (Symbol "foo")))))))
            , test "TEST 1.14: One final test to see that quote expansion works." <|
                \_ ->
                    parse "'(this ''''(makes ''no) 'sense)"
                        |> Expect.equal
                            (Ok
                                (Quote
                                    (ExpList
                                        [ Ref (Symbol "this")
                                        , Quote (Quote (Quote (Quote (ExpList [ Ref (Symbol "makes"), Quote (Quote (Ref (Symbol "no"))) ]))))
                                        , Quote (Ref (Symbol "sense"))
                                        ]
                                    )
                                )
                            )
            ]
        ]



--
-- def test_parse_larger_example():
--     """TEST 1.11: Test a larger example to check that everything works
--     as expected"""
--     program = """
--         (define fact
--         ;; Factorial function
--         (lambda (n)
--             (if (<= n 1)
--                 1 ; Factorial of 0 is 1, and we deny
--                   ; the existence of negative numbers
--                 (* n (fact (- n 1))))))
--     """
--     ast = ['define', 'fact',
--            ['lambda', ['n'],
--             ['if', ['<=', 'n', 1],
--              1,
--              ['*', 'n', ['fact', ['-', 'n', 1]]]]]]
--     assert_equals(ast, parse(program))

# Parser Talk

## Slides

1. DIY lang (anta alle er kjent)
   - la oss parse '(define x (if #t 42 0))'
2. elm-parser
   - operatorer etc.
3. presenter datastrukturer
   - hva trenger vi? exp? Symbol, Bool, Integer, ExpList
   - hvordan representere define og if
   - rekurisivt, se Exp
4. parse Value
   - Integer
   - Boolean
5. parse Ref
   - Symbol
6. parse ExpList
   - loop/helper function
7. parse If
8. parse define
9. demo!
